var express = require('express');
var router = express.Router();
var getCourses = require('../db');

/* GET course page. */
router.get('/:subj/:crse', function(req, res, next) {
    getCourses({ subject: req.params['subj'], course: parseInt(req.params['crse'], 10)},{},function(result) {
        res.render('course', { result });
    });
});

module.exports = router;
