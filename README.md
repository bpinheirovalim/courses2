# courses2

In order for this application to work, you are going to need a
collection called "courses" in MongoDB Atlas or similar with
documents with the following shape:

```json
{
    subject: "CPSC",
    course: 1030,
    title: "Web Development I",
    credits: 3,
    description: "Students will examine the structure of the Internet and the World Wide..."
}
```
Then you should be able to install the dependencies and run the
application as follows:

```shell
npm install
export MONGODBURI='mongodb+srv://<USERNAME>:<PASSWORD>@<HOSTNAME>'
export MONGODBNAME='<DBNAME>'
PORT=NNNN DEBUG=courses2:* npm run dev
```
